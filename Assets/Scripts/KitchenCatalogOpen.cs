﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitchenCatalogOpen : MonoBehaviour
{
    public GameObject KitchenCatalogImage;
    public GameObject closeButton;

    // Start is called before the first frame update
    void Start()
    {
        KitchenCatalogImage.SetActive(false);
        closeButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnClick()
    {
        KitchenCatalogImage.SetActive(true);
        closeButton.SetActive(true);
    }
}