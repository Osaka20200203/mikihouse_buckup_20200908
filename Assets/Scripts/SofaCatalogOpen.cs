﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SofaCatalogOpen : MonoBehaviour
{
    public GameObject sofaCatalogImage;

    // Start is called before the first frame update
    void Start()
    {
        sofaCatalogImage.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick()
    {
        sofaCatalogImage.SetActive(true);
    }
}
