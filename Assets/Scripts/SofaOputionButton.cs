﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SofaOputionButton : MonoBehaviour
{
    public GameObject sofaButton;

    // Start is called before the first frame update
    void Start()
    {
        sofaButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        sofaButton.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        sofaButton.SetActive(false);
    }
}
