﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitchenOptionButton : MonoBehaviour
{
    public GameObject KitchenButton;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Kitchenボタンオフ");
        KitchenButton.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        KitchenButton.SetActive(true);

    }

    private void OnTriggerExit(Collider other)
    {
        KitchenButton.SetActive(false);
    }
}
