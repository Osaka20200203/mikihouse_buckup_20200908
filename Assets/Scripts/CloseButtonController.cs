﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseButtonController : MonoBehaviour
{
    public GameObject closeButton;
    public GameObject sofaCatalogImage;

    // Start is called before the first frame update
    void Start()
    {
        closeButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnClick()
    {
        closeButton.SetActive(false);
        sofaCatalogImage.SetActive(false);
    }
}
