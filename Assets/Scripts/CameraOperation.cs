﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOperation : MonoBehaviour
{
    public float zoomSpeed = 20.0f;
    float rotationX;
    float rotationY;
    Camera camOperation;

    // Start is called before the first frame update
    void Start()
    {
        camOperation = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        // マウスのズーム処理
        // マウスをスクロールしたときの値を変数に代入
        float scroll = Input.GetAxis("Mouse ScrollWheel");

        // FieldOfViewが値が大きければ縮小されるため-scrollでドラックしたときに反対の挙動にならないようにする。
        float view = camOperation.fieldOfView - scroll * zoomSpeed;

        // FieldOfViewが1～100までのあいだにズームするようにする
        camOperation.fieldOfView = Mathf.Clamp(view, 1, 100);


        // マウスの角度処理
        if (Input.GetMouseButton(0))
        {
            rotationX = Input.GetAxis("Mouse Y");
            rotationY = Input.GetAxis("Mouse X");
            transform.Rotate(rotationX, rotationY, 0);
        }
    }
}
